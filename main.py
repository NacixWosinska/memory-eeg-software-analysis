import csv

import mne
from mne.io import read_raw_edf
import pyedflib
import numpy as np
import matplotlib.pyplot as pp
from scipy import signal
from numpy.fft import fftfreq
from scipy.fft import fft
from brainflow.board_shim import BoardShim, BoardIds
from brainflow.data_filter import DataFilter, NoiseTypes, WindowOperations

file_name = "2022_12_06T16_48_18.131Z.edf"
csv_name = "MARKERS_2022_12_06T16_48_18.131Z.csv"
f = pyedflib.EdfReader(file_name)

signals_amount = f.signals_in_file
signal_labels = f.getSignalLabels()
sampling_rate = 250

board_id = BoardIds.CYTON_BOARD.CYTON_BOARD
board_descr = BoardShim.get_board_descr(board_id)

start_time = 0
end_time = f.getNSamples()[0] / sampling_rate
start_time_samples = int(start_time * sampling_rate)
end_time_samples = int(end_time * sampling_rate)
number_of_samples = end_time_samples - start_time_samples


def get_time_steps(start_time=0, end_time=f.getNSamples()[0] / sampling_rate):
    time_steps = np.linspace(start_time, end_time, number_of_samples)
    return time_steps


def get_raw_data():
    sigbufs = np.zeros((signals_amount, number_of_samples))
    for i in np.arange(signals_amount):
        sigbufs[i, :] = f.readSignal(i, start_time_samples, number_of_samples)
    return sigbufs


def get_signal(i, sigbufs):
    return sigbufs[i]


def detrend_and_remove_noise():
    signals = np.zeros((signals_amount, number_of_samples))
    nfft = DataFilter.get_nearest_power_of_two(sampling_rate)

    for i in range(signals_amount):
        signals[i] = get_signal(i, get_raw_data())

        signals[i] = signal.detrend(signals[i])
        DataFilter.remove_environmental_noise(signals[i], sampling_rate,
                                              NoiseTypes.FIFTY.value)
        #
        # pp.figure()
        # pp.plot(get_time_steps(start_time, end_time)[250:], signals[i][250:])
        # pp.xlabel('Time [s]')
        # pp.ylabel(signal_labels[i] + ' [uV]')
        # # pp.savefig('ch_' + ch_names[i] + '_all_freqs_EC.eps', 1200)
        # pp.show()

        psd = DataFilter.get_psd_welch(signals[i][250:], nfft, nfft // 2, sampling_rate,
                                       WindowOperations.BLACKMAN_HARRIS)
        band_power_alpha = DataFilter.get_band_power(psd, 7.0, 13.0)
        band_power_beta = DataFilter.get_band_power(psd, 14.0, 30.0)
        print(band_power_beta, band_power_alpha)

    return signals


def filter_by_freqs(low, high, signals, rate=sampling_rate):
    a, b = signal.iirfilter(6, [low / (rate / 2.0), high / (rate / 2.0)])
    filtered_signals = np.zeros((signals_amount, number_of_samples))

    for i in range(signals_amount):
        filtered_signals[i] = signal.filtfilt(a, b, signals[i])
        pp.figure()
        pp.plot(get_time_steps(700, end_time)[250:], filtered_signals[i][250:])
        pp.xlabel('Time [s]')
        pp.ylabel(signal_labels[i] + ' [uV], bandpass filter ' + str(low) + "-" + str(high) + "Hz")
        #pp.savefig('ch_' + ch_names[i] + '_all_freqs_EC.eps', 1200)
        pp.show()

    return filtered_signals


def read_csv_file():
    with open(csv_name) as csvfile:
        csv_file = csv.reader(csvfile)
        csv_indexes = []
        is_remembered_csv_array = []
        type_csv_array = []

        for row in csv_file:
            csv_indexes.append(row[0])
            is_remembered_csv_array.append(row[2])
            type_csv_array.append(row[1])

        indexes = csv_indexes[1:]
        is_remembered_array = is_remembered_csv_array[1:]
        type_array = type_csv_array[1:]
    return indexes, is_remembered_array, type_array


def get_stimulus_window(indexes, type_array, is_remembered_array, sigbufs):
    stimulus_indexes = []
    is_stimulus_remembered = []

    for i in range(len(type_array)):
        if type_array[i] == "GET":
            stimulus_indexes.append(indexes[i])
            if is_remembered_array[i] == 'true':
                is_stimulus_remembered.append(1)
            else:
                is_stimulus_remembered.append(0)

    array = [0, 0, 0]

    for j in range(len(stimulus_indexes)):
        start_window_sample = int(stimulus_indexes[j])
        end_window_sample = int(stimulus_indexes[j]) + 250
        sigbufs_window = sigbufs[start_window_sample:end_window_sample]
        new_row = [int(stimulus_indexes[j]), 0, is_stimulus_remembered[j]]
        array = np.vstack([array, new_row])
        pp.figure()
        pp.plot(sigbufs_window)
        pp.show()

    return sigbufs, array[1:]

#
get_stimulus_window(read_csv_file()[0], read_csv_file()[2],
                     read_csv_file()[1], get_signal(1, filter_by_freqs(1, 35, detrend_and_remove_noise(), 250)))


def get_baseline_correction(stimulus_indexes, sigbufs):
    stimulus_windows = np.zeros(len(stimulus_indexes))
    for i in range(stimulus_indexes):
        start_window_sample = stimulus_indexes[i] - 125
        end_window_sample = stimulus_indexes[i] - 80
        sigbufs = sigbufs - sigbufs[start_window_sample:end_window_sample].mean()

    return sigbufs

filter_by_freqs(7, 13 , detrend_and_remove_noise())


def get_fft(signals):
    for i in range(signals_amount):
        N = signals[i].size

        yf = fft(signals[i])
        xf = fftfreq(N, 1 / sampling_rate)
        pp.figure()
        pp.plot(xf, np.abs(yf))
        pp.xlabel("Frequency [Hz]")
        pp.ylabel("Amplitude [uV]")
        pp.xlim(4, 11)
        pp.show()

# get_fft(filter_and_detrend())


# for j in range(len(sigbufs_opened_eyes)):
#     epoch_data = (2300, 8, 1)
#     bands_range = np.array([4, 7])
#     morlet = mne.time_frequency.tfr_morlet(filtered_sigbufs, 250, bands_range, 3, None, False)
